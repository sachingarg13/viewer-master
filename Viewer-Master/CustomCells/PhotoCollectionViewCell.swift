//
//  PhotoCollectionViewCell.swift
//  Viewer-Master
//
//  Created by Sachin Garg on 4/23/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setrecordCellWith(record: Records) {
        
        DispatchQueue.main.async {
            
                        if let url = record.icon_url {
                            self.imgView.loadImageUsingCacheWithURLString(url, placeHolder: UIImage(named: "PlaceHolder"))
                        }
        }
    }
}
