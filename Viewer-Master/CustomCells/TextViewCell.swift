//
//  TextViewCell.swift
//  Viewer-Master
//
//  Created by Sachin Garg on 4/23/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell {

  @IBOutlet weak var titleLabel: UILabel! {
    didSet {
      titleLabel.textColor = UIColor.darkGray
      titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
    }
  }

  @IBOutlet weak var descLabel: UILabel! {
    didSet {
      descLabel.textColor = UIColor.lightGray
      descLabel.font = UIFont.systemFont(ofSize: 12)
    }
  }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setrecordCellWith(record: Records) {
        
        DispatchQueue.main.async {
            self.titleLabel?.text = record.title
            self.descLabel?.text = record.desc


        }
    }
}
