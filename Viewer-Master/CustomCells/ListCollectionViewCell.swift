//
//  ListCollectionViewCell.swift
//  Viewer-Master
//
//  Created by Sachin Garg on 4/23/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setrecordCellWith(record: Records) {
        
        DispatchQueue.main.async {
            self.lbltitle?.text = record.title
            self.lblDesc?.text = record.desc
            //            if let url = photo.mediaURL {
            //                self.photoImageview.loadImageUsingCacheWithURLString(url, placeHolder: UIImage(named: "placeholder"))
            //            }
        }
    }
}
