//
//  Records+CoreDataProperties.swift
//  Viewer-Master
//
//  Created by Sachin Garg on 4/23/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//
//

import Foundation
import CoreData


extension Records {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Records> {
        return NSFetchRequest<Records>(entityName: "Records")
    }

    @NSManaged public var title: String?
    @NSManaged public var desc: String?
    @NSManaged public var icon_url: String?

}
