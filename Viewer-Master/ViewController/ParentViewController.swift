//
//  ParentViewController.swift
//  Viewer-Master
//
//  Created by Sachin Garg on 4/23/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {

    @IBOutlet weak var btnSwitchView: UIButton!
    @IBOutlet weak var ViewtogglePanel: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TableView") as! MainViewController
        self.ViewtogglePanel.addSubview(vc.view)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnToggleTap(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
