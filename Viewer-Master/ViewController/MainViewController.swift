//
//  ViewController.swift
//  Viewer-Master
//
//  Created by Sachin Garg on 4/23/19.
//  Copyright © 2019 Sachin Garg. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController {
    @IBOutlet weak var CollectionView: UIView!
    
  @IBOutlet weak var btnChangeView: UIButton! {
    didSet {
      toggleTopButton()      
    }
  }

  @IBOutlet weak var tableview: UITableView! {
    didSet {
      tableview.estimatedRowHeight = 120
      tableview.backgroundColor = .clear
      tableview.backgroundView = .none
      tableview.rowHeight = UITableView.automaticDimension
      tableview.estimatedSectionHeaderHeight = 90
      tableview.sectionHeaderHeight = UITableView.automaticDimension
    }
  }
    private let cellID = "TextViewCell"
    var  isGrid = false
    @IBAction func btnSwitch(_ sender: Any) {
        
        if  !isGrid{
            CollectionView.isHidden = false
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "GridView") as! GridCollectionViewController
            self.CollectionView.addSubview(vc.view)
        }else
        {
            
            CollectionView.isHidden = true

        }
        
        isGrid = !isGrid

      toggleTopButton()
    }
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Records.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: DatabaseCoreObjects.sharedInstance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Photos Feed"
        registerCells()
        view.backgroundColor = .white
        //tableview.register(TextViewCell.self, forCellReuseIdentifier: cellID)
        updateTableContent()
    }
    func registerCells() {
        tableview.register(UINib.init(nibName: "TextViewCell", bundle: nil), forCellReuseIdentifier: cellID)
    }
    func updateTableContent() {
        
        do {
            try self.fetchedhResultController.performFetch()
            print("COUNT FETCHED FIRST: \(String(describing: self.fetchedhResultController.sections?[0].numberOfObjects))")
        } catch let error  {
            print("ERROR: \(error)")
        }
        
        let service = APIServiceCalls()
        service.getDataWith { (result) in
            switch result {
            case .Success(let data):
                self.clearData()
                self.saveInCoreDataWith(array: data)
            case .Error(let message):
                DispatchQueue.main.async {
                    self.showAlertWith(title: "Error", message: message)
                }
            }
        }
    }
    
    func showAlertWith(title: String, message: String, style: UIAlertController.Style = .alert) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let action = UIAlertAction(title: title, style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }

  fileprivate func toggleTopButton() {
    if isGrid == true {
      btnChangeView.setTitle("List View", for: .normal)
    } else {
      btnChangeView.setTitle("Grid View", for: .normal)
    }
  }

    private func createPhotoEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
        
        let context = DatabaseCoreObjects.sharedInstance.persistentContainer.viewContext
        if let RecordEntity = NSEntityDescription.insertNewObject(forEntityName: "Records", into: context) as? Records {
//            RecordEntity.title =  dictionary["Result"] as? String
//            RecordEntity.desc = dictionary["Text"] as? String
//            let mediaDictionary = dictionary["Icon"] as? [String: AnyObject]
//            RecordEntity.icon_url = mediaDictionary?["URL"] as? String
            
            RecordEntity.desc = dictionary["Text"] as? String
            let str =  dictionary["Text"] as? String
            
            let list = str?.split(separator: "-", maxSplits: 2, omittingEmptySubsequences: true)
            RecordEntity.title = String(list?.first ?? "") //list?.first
            
            let mediaDictionary = dictionary["Icon"] as? [String: AnyObject]
            RecordEntity.icon_url = mediaDictionary?["URL"] as? String
            return RecordEntity
        }
        return nil
    }
    
    private func saveInCoreDataWith(array: [[String: AnyObject]]) {
        _ = array.map{self.createPhotoEntityFrom(dictionary: $0)}
        do {
            try DatabaseCoreObjects.sharedInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    private func clearData() {
        do {
            
            let context = DatabaseCoreObjects.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Records.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                DatabaseCoreObjects.sharedInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
}

extension MainViewController : UITableViewDataSource
{
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! TextViewCell
        
        if let Record = fetchedhResultController.object(at: indexPath) as? Records {
            
            cell.setrecordCellWith(record: Record)
        }
        return cell
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = fetchedhResultController.sections?.first?.numberOfObjects {
            return count
        }
        return 0
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        //return UItableviewh //100 = sum of labels height + height of divider line
    }
}

extension MainViewController: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.tableview.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.tableview.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableview.endUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableview.beginUpdates()
    }
}





